extends Area

signal coinColected

func _ready():
	pass

# warning-ignore:unused_argument
func _physics_process(delta):
	rotate_y(deg2rad(3))


func _on_coin_body_entered(body):
	if body.name == "Alex":
		$AnimationPlayer.play("bounce")
		$Timer.start()

func _on_Timer_timeout():
	emit_signal("coinColected")
	queue_free()
